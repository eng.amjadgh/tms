<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Carbon\Carbon;

class TaskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'task_description' => $this->faker->name(),
            'created_date' => Carbon::now()->toDateTimeString(),
            'deadline_date' => Carbon::now()->toDateString(),
            'subtask' => $this->faker->name(),
            'assign_id' => 1,
            'user_id' => 1,
        ];
    }
}
