<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class CategorySeeder extends Seeder
{
    public function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }
    
    public function random_color() {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1;$i<=15;$i++) {
            DB::table('categories')->insert([
                'name' => "Category - ".$i,
                'color' => "#".$this->random_color(),
                'created_at' => Carbon::now()->toDateTimeString(),
            ]);
            $i = $i+1;
        }
    }
}
