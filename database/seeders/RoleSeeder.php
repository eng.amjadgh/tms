<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => "Admin",
            'created_at' => Carbon::now()->toDateTimeString(),
        ]);
        DB::table('roles')->insert([
            'name' => "User",
            'created_at' => Carbon::now()->toDateTimeString(),
        ]);
    }
}
