<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Task::factory(25)->create();
        $i=0;
        while($i<50) {    
            DB::table('tasks_categories')->insert([
                'task_id' => rand(1,25),
                'category_id' => rand(1,8),
            ]);
            $i++;
        }
    }
}
