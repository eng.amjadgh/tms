<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use DB;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Admin",
            'email' => "gneemamjad@gmail.com",
            'password' => Hash::make("password"),
            'created_at' => Carbon::now()->toDateTimeString(),
            'role_id' => 1,
        ]);
    }
}
