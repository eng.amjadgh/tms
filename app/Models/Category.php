<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'color',
    ];

    public function tasks(){
        return $this->belongsToMany(Task::class, 'tasks_categories', 'category_id', 'task_id');
    }
}
