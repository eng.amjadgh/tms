<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'task_description',
        'created_date',
        'deadline_date',
        'subtask',
        'end_flag',
        'assign_id',
        'user_id',
    ];
    
    public function categories(){
        return $this->belongsToMany(Category::class, 'tasks_categories', 'task_id', 'category_id');
    }
    
    public function assign(){
        return $this->belongsTo(User::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
