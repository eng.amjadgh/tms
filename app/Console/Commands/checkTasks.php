<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Carbon\Carbon;
use App\Jobs\SendEmailJob;
use App\Models\Task;

class checkTasks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:checkTasks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command use to check task deadline';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $arr = DB::table('tasks')->whereDate('deadline_date', '<', Carbon::today())
        ->where('end_flag',0)
        ->get();
        $arrayofobj = array();
        
        foreach($arr as $item)
            array_push($arrayofobj,$item);

        foreach($arrayofobj as $task) {
            $taskobject = Task::find($task->id);
            $taskobject->assign;
            DB::table('tasks')->where('id', $task->id)->update(['end_flag'=>'1']);
            $details['email'] = $taskobject->assign->email;
            dispatch(new SendEmailJob($details));
        }
    }
}
