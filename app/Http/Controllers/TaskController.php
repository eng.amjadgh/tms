<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;
use App\Models\Category;
use App\Models\User;
use DataTables;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{

    public function checkadmin($id) {
        $usrobj = User::findorfail($id);
        $usrobj->role->name;
        return $usrobj->role->name;
    }
    public function index(){
        $categories = Category::all();
        return view('tasks.index')
        ->with('categories', $categories);
        
    }
    public function getTasks(Request $request)
    {
        if ($request->ajax()) {
            $userid = $request->get('userid');
            if($this->checkadmin($userid) == 'Admin') {
                $tasks = Task::select('id','task_description','deadline_date','end_flag','user_id')->get();
            } else {
                $tasks = Task::select('id','task_description','deadline_date','end_flag','user_id')->where('user_id',$userid)->orWhere('assign_id',$userid)->get();
            }
            $tasksarray = array();
            foreach($tasks as $task) {
                $taskobj = Task::findorfail($task->id);
                if($taskobj->end_flag == 1) {
                    $taskobj->end_flag = 'Complete';
                } else {
                    $taskobj->end_flag = 'In Progress';
                }
                if($taskobj->assign) {
                    $taskobj->assignname = $taskobj->assign->name;
                }
                if($taskobj->user) {
                    $taskobj->username = $taskobj->user->name;
                }
                $first = true;
                $taskobj->categoriesarray = "";
                foreach($taskobj->categories as $cat) {
                    if($first) {
                        $taskobj->categoriesarray = $taskobj->categoriesarray.$cat->name;
                        $first = false;
                    } else {
                        $taskobj->categoriesarray = $taskobj->categoriesarray.", ".$cat->name;
                    }
                }
                array_push($tasksarray, $taskobj);
            }
            
            return Datatables::of($tasksarray)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn='';
                    if($row->userrole == 'Admin') {
                        $actionBtn = '<a href="/tasks/'.$row->id.'" class="edit btn btn-primary btn-sm">View</a>
                        <a href="/tasks/edit/'.$row->id.'" class="edit btn btn-success btn-sm">Edit</a>
                        <a href="/tasks/delete/'.$row->id.'" class="delete btn btn-danger btn-sm">Delete</a>';
                    } else if(Auth::user()->id == $row->assign_id && Auth::user()->id != $row->user_id) {
                        $actionBtn = '<a href="/tasks/'.$row->id.'" class="edit btn btn-primary btn-sm">View</a>';
                    } else if(Auth::user()->id == $row->user_id) {
                        $actionBtn = '<a href="/tasks/'.$row->id.'" class="edit btn btn-primary btn-sm">View</a>
                        <a href="/tasks/edit/'.$row->id.'" class="edit btn btn-success btn-sm">Edit</a>
                        <a href="/tasks/delete/'.$row->id.'" class="delete btn btn-danger btn-sm">Delete</a>';
                    }
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function getTask($id) {
        $taskobj = Task::find($id);
        if($taskobj) {
            if($this->checkadmin(Auth::user()->id) == 'Admin' || Auth::user()->id == $taskobj->user_id ||  Auth::user()->id == $taskobj->assign_id) {
                if($taskobj->end_flag == 1) {
                    $taskobj->end_flag = 'Complete';
                } else {
                    $taskobj->end_flag = 'In Progress';
                }
                if($taskobj->assign) {
                    $taskobj->assignname = $taskobj->assign->name;
                }
                if($taskobj->user) {
                    $taskobj->username = $taskobj->user->name;
                }
                $first = true;
                $taskobj->categoriesarray = "";
                foreach($taskobj->categories as $cat) {
                    if($first) {
                        $taskobj->categoriesarray = $taskobj->categoriesarray.$cat->name;
                        $first = false;
                    } else {
                        $taskobj->categoriesarray = $taskobj->categoriesarray.", ".$cat->name;
                    }
                }
                return view('tasks.show')
                ->with('task', $taskobj);
            } else {
                return redirect()->route('tasks');
            }
        } else {
            return redirect()->route('tasks');
        }
    }

    public function create() {
        $categories = Category::all();
        $users = User::all();
        if(Auth::user()) {
            return view('tasks.create')
            ->with('categories', $categories)
            ->with('users', $users);
        } else {
            return redirect()->route('login');
        }
    }

    public function store(Request $request) {
        $categories = Category::all();
        if(Auth::user()) {
            $this->validate($request, array(
                'task_description' => 'required|string|max:191',
                'created_date' => 'required|date',
                'deadline_date' => 'required|date',            
                'subtask' => 'required|string',
                'end_flag' => 'required|int',
                'assign_id' => 'required|int'
            ));
            // save
            $taskobj = Task::create($request->all());
            $taskobj2 = Task::find($taskobj->id);
            foreach ($request['categoriesarray'] as $selectedOption)
                $taskobj->categories()->attach($selectedOption);
            $taskobj->user_id = Auth::user()->id;
            $taskobj->save();
        
            
            return redirect()->route('tasks');
        } else {
            return redirect()->route('tasks');
        }
    }


    public function edit($id) {
        $categories = Category::all();
        $users = User::all();
        $taskobj = Task::find($id);
        if($taskobj) {
            if($this->checkadmin(Auth::user()->id) == 'Admin' || Auth::user()->id == $taskobj->user_id ||  Auth::user()->id == $taskobj->assign_id) {
                if($taskobj->end_flag == 1) {
                    $taskobj->end_flag = 'Complete';
                } else {
                    $taskobj->end_flag = 'In Progress';
                }
                if($taskobj->assign) {
                    $taskobj->assignname = $taskobj->assign->name;
                }
                if($taskobj->user) {
                    $taskobj->username = $taskobj->user->name;
                }
                $first = true;
                $taskobj->categoriesarray = "";
                foreach($taskobj->categories as $cat) {
                    if($first) {
                        $taskobj->categoriesarray = $taskobj->categoriesarray.$cat->name;
                        $first = false;
                    } else {
                        $taskobj->categoriesarray = $taskobj->categoriesarray.", ".$cat->name;
                    }
                }
                return view('tasks.edit')
                ->with('task', $taskobj)
                ->with('categories', $categories)
                ->with('users', $users);
            } else {
                return redirect()->route('tasks');
            }
        } else {
            return redirect()->route('tasks');
        }
    }

    public function update(Request $request, $id) {
        $taskobj = Task::find($id);
        $categories = Category::all();
        if($taskobj) {
            if($this->checkadmin(Auth::user()->id) == 'Admin' || Auth::user()->id == $taskobj->user_id) {
                
                $this->validate($request, array(
                    'task_description' => 'required|string|max:191',
                    'created_date' => 'required|date',
                    'deadline_date' => 'required|date',            
                    'subtask' => 'required|string',
                    'end_flag' => 'required|int',
                    'assign_id' => 'required|int'
                ));
                // save
                foreach ($categories as $cat) {
                    $taskobj->categories()->detach($cat->id);
                }
                foreach ($request['categoriesarray'] as $selectedOption)
                    $taskobj->categories()->attach($selectedOption);
                $taskobj->task_description = $request['task_description'];
                $taskobj->created_date = $request['created_date'];
                $taskobj->deadline_date = $request['deadline_date'];
                $taskobj->subtask = $request['subtask'];
                $taskobj->end_flag = $request['end_flag'];
                $taskobj->assign_id = $request['assign_id'];
                $taskobj->save();
            
                
                return redirect()->route('tasks');
            } else {
                return redirect()->route('tasks');
            }
        } else {
            return redirect()->route('tasks');
        }
    }

    public function delete($id) {
        $taskobj = Task::find($id);
        if($taskobj) {
            if($this->checkadmin(Auth::user()->id) == 'Admin' || Auth::user()->id == $taskobj->user_id) {
                $taskobj->delete();
                return redirect()->route('tasks');
            } else {
                return redirect()->route('tasks');
            }
        } else {
            return redirect()->route('tasks');
        }
    }
}
