@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Task Details') }}</div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <form action="{!! route('tasks.store') !!}" method="post">
                                @csrf <!-- {{ csrf_field() }} -->
                                <input type="text" hidden name='user_id' value="{{auth()->user()->id}}">
                                <div class="form-group">
                                    <label for="Task_Description">Task Description</label>
                                    <input type="text" class="form-control" id="Task_Description" name="task_description" placeholder="Task Description">
                                </div>
                                <div class="form-group">
                                    <label for="SubTask">Sub Tasks</label>
                                    <textarea class="form-control" id="SubTask" name="subtask" rows="4"></textarea>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-4">
                                            <label for="Category">Select multiple Category</label>
                                            <select multiple class="form-control" id="Category" name="categoriesarray[]">
                                                @foreach($categories as $cat)
                                                    <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-4">
                                            <label for="Assigne">Assigne select</label>
                                            <select class="form-control" id="Assigne" name="assign_id">
                                                @foreach($users as $usr)
                                                    <option value="{{$usr->id}}">{{$usr->name}}</option>
                                                @endforeach
                                                
                                            </select>
                                            <hr style="margin-bottom:0.5rem;margin-top:0.75rem">
                                            <label for="EndFlag">End Flag</label>
                                            <select class="form-control" id="EndFlag" name="end_flag">
                                                <option value="1">Completed</option>
                                                <option value="0">In Progress</option>
                                            </select>
                                        </div>
                                        <div class="col-4">
                                            <label for="createdDate">Created Time</label>
                                            <input class="form-control" id="createdDate" type="datetime-local" name="created_date">
                                            <hr style="margin-bottom:0.5rem;margin-top:0.75rem">
                                            <label for="deadlineDate">DeadLine Time</label>
                                            <input class="form-control" id="deadlineDate" type="date" name="deadline_date">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-lg"> Create</button>
                                    <a href="/tasks" class="btn btn-danger btn-lg"> Back</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection