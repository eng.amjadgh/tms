@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2">
            <div class="card">
                <div class="card-header">Categories</div>
                <div class="card-body">
                    <ul>
                        @foreach($categories as $cat)
                            <li style='color:{{$cat->color}}'>{{$cat->name}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Tasks</div>
                <div class="card-body">
                    <table class="table table-bordered task-datatable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Task Description</th>
                                <th>DeadLine</th>
                                <th>Status</th>
                                <th>Categories</th>
                                <th>Assign ID</th>
                                <th>Controller</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer-scripts')
    @include('scripts.task-script')
@endsection