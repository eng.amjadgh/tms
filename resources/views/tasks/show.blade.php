@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Task Details') }}</div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-8">
                            <h3>{{$task->task_description}} <span class="endflagstyle" style="<?php echo ($task->end_flag == 'Complete') ? 'color:green' : 'color:gray'; ?>">{{$task->end_flag}}</span></h3>
                            <p>{{$task->subtask}}</p>
                            <p>{{$task->categoriesarray}}</p>
                        </div>
                        <div class="col-4 daterow">
                            <h5 class="date"><span>Created At:</span> {{$task->created_date}}</h5>
                            <h5 class="date"><span>DeadLine At:</span> {{$task->deadline_date}}</h5>
                            <h5 class="date"><span>Created By:</span> {{$task->username}}</h5>
                            <h5 class="date"><span>Assigned To:</span> {{$task->assignname}}</h5>
                            <?php
                            if($task->userrole == 'Admin') {
                                $actionBtn = '
                                <a href="/tasks/edit/'.$task->id.'" class="edit btn btn-success btn-sm">Edit</a>
                                <a href="/tasks/delete/'.$task->id.'" class="delete btn btn-danger btn-sm">Delete</a>';
                            } else if(Auth::user()->id == $task->user_id) {
                                $actionBtn = '
                                <a href="/tasks/edit/'.$task->id.'" class="edit btn btn-success btn-sm">Edit</a>
                                <a href="/tasks/delete/'.$task->id.'" class="delete btn btn-danger btn-sm">Delete</a>';
                            } else {
                                $actionBtn="";
                            }
                            echo $actionBtn;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
