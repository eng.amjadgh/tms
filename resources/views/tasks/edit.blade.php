@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Task Details') }}</div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <form action="{!! route('tasks.update',['id' => $task->id]) !!}" method="post">
                                @csrf <!-- {{ csrf_field() }} -->
                                <div class="form-group">
                                    <label for="Task_Description">Task Description</label>
                                    <input type="text" class="form-control" id="Task_Description" name="task_description" placeholder="Task Description" value="{{$task->task_description}}">
                                </div>
                                <div class="form-group">
                                    <label for="SubTask">Sub Tasks</label>
                                    <textarea class="form-control" id="SubTask" name="subtask" rows="4">{{$task->subtask}}</textarea>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-4">
                                            <label for="Category">Select multiple Category</label>
                                            <select multiple class="form-control" id="Category" name="categoriesarray[]">
                                                <?php 
                                                $catss = $task->categoriesarray;
                                                $catss_ar = explode(', ', $catss);
                                                ?>
                                                @foreach($categories as $cat)
                                                    <option <?php if(in_array($cat->name,$catss_ar)) echo 'selected'; ?> value="{{$cat->id}}">{{$cat->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-4">
                                            <label for="Assigne">Assigne select</label>
                                            <select class="form-control" id="Assigne" name="assign_id">
                                                @foreach($users as $usr)
                                                    <option <?php if($usr->id == $task->assign_id) echo 'selected'; ?> value="{{$usr->id}}">{{$usr->name}}</option>
                                                @endforeach
                                                
                                            </select>
                                            <hr style="margin-bottom:0.5rem;margin-top:0.75rem">
                                            <label for="EndFlag">End Flag</label>
                                            <select class="form-control" id="EndFlag" name="end_flag">
                                                <option <?php if($task->end_flag == "Complete") echo 'selected'; ?> value="1">Completed</option>
                                                <option <?php if($task->end_flag == "In Progress") echo 'selected'; ?> value="0">In Progress</option>
                                            </select>
                                        </div>
                                        <div class="col-4">
                                            <label for="createdDate">Created Time</label>
                                            <?php 
                                                // create a $dt object with the UTC timezone
                                                $dt = new DateTime($task->created_date, new DateTimeZone('T'));
                                            ?>
                                            <input class="form-control" id="createdDate" type="datetime-local" name="created_date" value="<?php echo $dt->format('Y-m-dTH:i:s'); ?>">
                                            <hr style="margin-bottom:0.5rem;margin-top:0.75rem">
                                            <label for="deadlineDate">DeadLine Time</label>
                                            <input class="form-control" id="deadlineDate" type="date" name="deadline_date" value="{{$task->deadline_date}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-lg"> Edit</button>
                                    <a href="/tasks" class="btn btn-danger btn-lg"> Back</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection