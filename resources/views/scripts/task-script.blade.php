<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
  $(function () {
    var formData = {userid:"{{Auth::user()->id}}"};
    var table = $('.task-datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ route('tasks.list') }}",
            type: "POST",
            data: formData,
        },
        columnDefs: [
            { width: 400, targets: 1 },
        ],
        fixedColumns: true,
        columns: [
            {data: 'id', name: 'DT_RowIndex'},
            {data: 'task_description', name: 'task_description'},
            {data: 'deadline_date', name: 'deadline_date'},
            {data: 'end_flag', name: 'end_flag'},
            {data: 'categoriesarray', name: 'categoriesarray'},
            {data: 'assignname', name: 'assignname'},
            {
                data: 'action', 
                name: 'action', 
                orderable: true, 
                searchable: true
            },
        ]
    });
    
  });
</script>