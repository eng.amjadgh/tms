<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::group(['middleware' => 'auth:sanctum'], function() {
    Route::post('tasks/list', [App\Http\Controllers\TaskController::class, 'getTasks'])->name('tasks.list');

    Route::get('/tasks', [App\Http\Controllers\TaskController::class, 'index'])->name('tasks');
    Route::get('/tasks/create/', [App\Http\Controllers\TaskController::class, 'create'])->name('tasks.create');
    Route::post('/tasks/store', [App\Http\Controllers\TaskController::class, 'store'])->name('tasks.store');
    Route::get('/tasks/{id}', [App\Http\Controllers\TaskController::class, 'getTask']);
    Route::get('/tasks/edit/{id}', [App\Http\Controllers\TaskController::class, 'edit']);
    Route::post('/tasks/update/{id}', [App\Http\Controllers\TaskController::class, 'update'])->name('tasks.update');
    Route::delete('/tasks/delete/{id}', [App\Http\Controllers\TaskController::class, 'delete']);
});